package com.example.BrewAssistantApi.Models;

import lombok.Data;

@Data
public class Mash {
//    public String id;
    public double totalMalt;
    public double mashVolume;
    public double lauteringWater;
}
