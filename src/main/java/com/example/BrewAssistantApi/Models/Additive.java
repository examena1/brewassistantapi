package com.example.BrewAssistantApi.Models;

import lombok.Data;

@Data
public class Additive {
    public  String  additive;
    public  String batchNr;
    public  String property;
    public  double gram;
}
