package com.example.BrewAssistantApi.Models;

import lombok.Data;

@Data
public class Timer {
    public String color;
    public String title;
    public double time;
    public Integer number;
}
