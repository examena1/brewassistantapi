package com.example.BrewAssistantApi.Models;

import com.example.BrewAssistantApi.Entities.MashEntity;
import lombok.Data;

import java.util.List;

@Data
public class Recipe {
    public BeerValues beerValues;
    public MashEntity mashVolume;
    public List<Additive> additives;
    public List<Malt> malts;
    public List<Hops> hops;
    public Volume volume;
    public List<Timer> timers;
    public String note;
}
