package com.example.BrewAssistantApi.Models;

import lombok.Data;

@Data
public class Hops {
    public String hops;
    public String batchNr;
    public double alphaAcid;
    public double parts;

}
