package com.example.BrewAssistantApi.Models;

import lombok.Data;

import java.util.Date;

@Data
public class BeerValues {
    public Date date;
    public String beerName;
    public String beerStyle;
    public String batchNr;
    public Double goalIbu;
    public Double expectedGravity;
}
