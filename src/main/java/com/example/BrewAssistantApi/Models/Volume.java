package com.example.BrewAssistantApi.Models;

import lombok.Data;

@Data
public class Volume {
    public double breBoil;
    public double afterBoil;
    public double gravityAfterBoiling;
    public double newVolume;
}
