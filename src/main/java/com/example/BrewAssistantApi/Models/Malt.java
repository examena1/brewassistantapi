package com.example.BrewAssistantApi.Models;

import lombok.Data;

@Data
public class Malt {
    public  String  malt;
    public  String batchNr;
    public  double parts;
//    public  double gram;

}
