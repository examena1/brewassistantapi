package com.example.BrewAssistantApi.Models;

import lombok.Data;

@Data
public class User {
    public String email;
}
