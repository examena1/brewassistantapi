package com.example.BrewAssistantApi.Controllers;

import com.example.BrewAssistantApi.Dto.StringDto;
import com.example.BrewAssistantApi.Dto.StringNoteDto;
import com.example.BrewAssistantApi.Entities.RecipeEntity;
import com.example.BrewAssistantApi.Services.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/recipe")
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @PostMapping("/create/{userId}")
    public StringDto createRecipe(@PathVariable String userId) {
      String id = recipeService.createRecipe(userId).orElse(null);

      return new StringDto(id);
    }

    @GetMapping("/all-recipes-from-user/{user_Id}")
    public List<RecipeEntity> getAlltRecipeFromUser(@PathVariable String user_Id) {
        return recipeService.geAllRecipeFromUser(user_Id);
    }

    @GetMapping("/all-recipes-from-db")
    public List<RecipeEntity> getAlltRecipeFromDb() {
       List<RecipeEntity>recipe= recipeService.allRecipesFromDb();
        return recipe;
    }
    @GetMapping("/all-recipes-from-db/{id}")
    public List<RecipeEntity> getRecipeFromDbById(@PathVariable String id) {
        return recipeService.recipeFromDbById(id).collect(Collectors.toList());
    }

    @GetMapping("/recipe-by-id/{recipe_Id}")
    public Optional<RecipeEntity> recipeById(@PathVariable String recipe_Id) {
        return Optional.ofNullable(recipeService.getSingelRecipeById(recipe_Id).orElse(null));
    }

    //-------note hatering
    @PutMapping("note/{recipe_Id}")
    public String writeNote(@PathVariable String recipe_Id,@RequestBody String note){
        return recipeService.writeNote(recipe_Id,note);
    }
    @PutMapping("note-empty/{recipe_Id}")
    public String  setNoteEmptyString(@PathVariable String recipe_Id){
        return recipeService.setNoteEmptyString(recipe_Id);
    }
    @GetMapping("get-note/{recipe_Id}")
    public StringNoteDto getNote(@PathVariable String recipe_Id) {
        String note = recipeService.getNote(recipe_Id);
        return new StringNoteDto(note);
    }

    @DeleteMapping("/delete-recipe/{recipeId}")
    public void deletedRecipe(@PathVariable String recipeId) {
        recipeService.deleteRecipe(recipeId);
    }


}
