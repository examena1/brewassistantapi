package com.example.BrewAssistantApi.Controllers;

import com.example.BrewAssistantApi.Dto.TimerDto;
import com.example.BrewAssistantApi.Entities.TimerEntity;
import com.example.BrewAssistantApi.Models.Timer;
import com.example.BrewAssistantApi.Services.TimerService;
import com.example.BrewAssistantApi.Util.UtilTimer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/timer")
public class TimerController {
    @Autowired
    private TimerService timerService;

    @PostMapping("/{recipe_Id}")
    public Optional<TimerDto> createTimer(@RequestBody Timer timer, @PathVariable String recipe_Id){
        return  timerService.createTimer(timer,recipe_Id).map(UtilTimer::EntityToDto);
    }

    @GetMapping("/all-timers-from-recipe/{recipe_id}")
    public List<TimerEntity> getAllAdditiveFromDRecipe(@PathVariable String recipe_id){
        return timerService.getAllTimersFromRecipe(recipe_id).collect(Collectors.toList());
    }

    @DeleteMapping("/{timer_id}")
    public void deleteTimer(@PathVariable String timer_id){
        timerService.deleteTimer(timer_id);
    }

}
