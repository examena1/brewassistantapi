package com.example.BrewAssistantApi.Controllers;

import com.example.BrewAssistantApi.Dto.HopsDto;
import com.example.BrewAssistantApi.Entities.HopsEntity;
import com.example.BrewAssistantApi.Models.Hops;
import com.example.BrewAssistantApi.Services.HopsService;
import com.example.BrewAssistantApi.Util.UtilHops;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/hops")
public class HopsController {

    @Autowired
    private HopsService hopsService;

    @PostMapping("/{recipe_Id}")
    public Optional<HopsDto> createHops(@RequestBody Hops hops, @PathVariable String recipe_Id){
        return  hopsService.createHops(hops,recipe_Id).map(UtilHops::EntityToDto);
    }

    @GetMapping("/all-hops-from-recipe/{recipe_id}")
    public List<HopsEntity> getAllAdditiveFromDRecipe(@PathVariable String recipe_id){
        return hopsService.getAllHopsFromRecipe(recipe_id).collect(Collectors.toList());
    }

    @GetMapping("/totalhopsgrams/{recipe_id}")
    public double getTotalHopsInMaltList(@PathVariable String recipe_id){
        return hopsService.totalGramOfHopsFromList(recipe_id).orElse(0.0);
    }



    @DeleteMapping("/{hops_id}")
    public void deleteHops(@PathVariable String hops_id){
        hopsService.deleteHops(hops_id);
    }

}
