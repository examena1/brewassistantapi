package com.example.BrewAssistantApi.Controllers;

import com.example.BrewAssistantApi.Dto.UserDto;
import com.example.BrewAssistantApi.Models.User;
import com.example.BrewAssistantApi.Services.UserService;
import com.example.BrewAssistantApi.Util.UtilUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public UserDto createUser(@RequestBody User user) {

        return userService.createUser(user)
                .map(UtilUser::EntityToDto)
                .orElse(null);
    }

    @PutMapping("/update/{id}")
    public UserDto updateUser(@RequestBody User user, @PathVariable String id) {

        return userService.updateUser(user,id)
                .map(UtilUser::EntityToDto)
                .orElse(null);
    }


    @GetMapping("/{id}")
    public UserDto getByUser( @PathVariable String id){
        return userService.getUserById(id).map(UtilUser::EntityToDto).orElse(null);
    };

    @GetMapping("/email/{email}")
    public UserDto getByUserEmail( @PathVariable String email){
        return userService.getUserByEmail(email).map(UtilUser::EntityToDto).orElse(null);
    };


    @GetMapping("/all")
    public List<UserDto> gettAllUsers( ){
        return userService.getAllUsers().map(UtilUser::EntityToDto).collect(Collectors.toList());
    };




    @DeleteMapping("/{id}")
    public void deleteByUser( @PathVariable String id){
        userService.deleteUserById(id);
    };



}
