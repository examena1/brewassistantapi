package com.example.BrewAssistantApi.Controllers;

import com.example.BrewAssistantApi.Dto.MaltDto;
import com.example.BrewAssistantApi.Entities.MaltEntity;
import com.example.BrewAssistantApi.Models.Malt;
import com.example.BrewAssistantApi.Services.MaltService;
import com.example.BrewAssistantApi.Util.UtilMalt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/malt")
public class MaltController {

    @Autowired
    private MaltService maltService;

    @PostMapping("/{recipe_Id}")
    public Optional<MaltDto> createMalt(@RequestBody Malt malt, @PathVariable String recipe_Id){
        return  maltService.createMalt(malt,recipe_Id).map(UtilMalt::EntityToDto);
    }

    @GetMapping("/all-malts-from-recipe/{recipe_id}")
    public List<MaltEntity> getAllAdditiveFromDRecipe(@PathVariable String recipe_id){
        return maltService.getAllMaltsFromRecipe(recipe_id).collect(Collectors.toList());
    }
    @GetMapping("/totalmaltgrams/{recipe_id}")
    public double getTotalMaltInMaltList(@PathVariable String recipe_id){
        return maltService.totaMaltFromMaltList(recipe_id).orElse(0.0);
    }



    @DeleteMapping("/{malt_id}")
    public void deleteMalt(@PathVariable String malt_id){
        maltService.deleteMalt(malt_id);
    }

}
