package com.example.BrewAssistantApi.Controllers;

import com.example.BrewAssistantApi.Dto.AdditivesDto;
import com.example.BrewAssistantApi.Models.Additive;
import com.example.BrewAssistantApi.Services.AdditiveService;
import com.example.BrewAssistantApi.Util.UtilAdditive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/additive")
public class AdditiveController {
    @Autowired
    private AdditiveService additiveService;

    @PostMapping("/{recipe_Id}")
    public Optional<AdditivesDto> createAdditive(@RequestBody Additive additive, @PathVariable String recipe_Id){
        return additiveService.createAdditive(additive,recipe_Id).map(UtilAdditive::EntityToDto);
    }

    @GetMapping("/all-additives-from-db")
    public List<AdditivesDto> getAllAdditiveFromDb(){
        return additiveService.getAllAdditivesFromDb().map(UtilAdditive::EntityToDto).collect(Collectors.toList());
    }

    @GetMapping("/all-additives-from-recipe/{recipe_id}")
    public List<AdditivesDto> getAllAdditiveFromDRecipe(@PathVariable String recipe_id){
        return additiveService.getAllAdditivesFromRecipe(recipe_id).map(UtilAdditive::EntityToDto).collect(Collectors.toList());
    }

    @DeleteMapping("/{additive_id}")
    public void deleteAdditive(@PathVariable String additive_id){
        additiveService.deleteAdditive(additive_id);
    }


}
