package com.example.BrewAssistantApi.Controllers;

import com.example.BrewAssistantApi.Dto.BeerValuesDto;
import com.example.BrewAssistantApi.Entities.BeerValuesEntity;
import com.example.BrewAssistantApi.Models.BeerValues;
import com.example.BrewAssistantApi.Services.BeerValuesService;
import com.example.BrewAssistantApi.Util.UtilBeerValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/beer-values")
public class BeerValuesController {

    @Autowired
    private BeerValuesService service;

    @PostMapping("/{recipeId}")
    public BeerValuesDto createBeerValues(@RequestBody BeerValues beerValues, @PathVariable String recipeId) {
        return service.createBeerValues(beerValues, recipeId).map(UtilBeerValues::EntityToDto).orElse(null);

    }

    @PutMapping("update/{recipe_Id}")
    public BeerValuesDto updateBeerValues(@RequestBody BeerValues beerValues, @PathVariable String recipe_Id) {
        return service.updateBeerValues(beerValues, recipe_Id).map(UtilBeerValues::EntityToDto).orElse(null);

    }

    @GetMapping
    public List<BeerValuesDto> getAllBeerValues() {
        return service.getAllBeerValues().map(UtilBeerValues::EntityToDto).collect(Collectors.toList());
    }

    @GetMapping("{values_Id}")
    public Optional<BeerValuesDto> getBeerValuesById(@PathVariable String  values_Id) {
        return service.getBeerValuesById(values_Id).map(UtilBeerValues::EntityToDto);
    }

    @DeleteMapping("{values_Id}")
    public void deleteValuesById(@PathVariable String values_Id) {
       service.deleteValuesById(values_Id);
    }


}
