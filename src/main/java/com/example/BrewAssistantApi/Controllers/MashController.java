package com.example.BrewAssistantApi.Controllers;

import com.example.BrewAssistantApi.Entities.MashEntity;
import com.example.BrewAssistantApi.Models.Mash;
import com.example.BrewAssistantApi.Services.MashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/mash")
public class MashController {

    @Autowired
    private MashService mashService;


    @PostMapping("/{recipe_Id}")
    public Optional<MashEntity> createMash(@RequestBody Mash mash, @PathVariable String recipe_Id) {
        return mashService.createMashValues(mash, recipe_Id);

    }

    @PutMapping("/{recipe__Id}")
    public Optional<MashEntity> updateMash(@RequestBody Mash mash, @PathVariable String recipe__Id) {
        return mashService.updateMash(mash, recipe__Id);

    }

    @GetMapping("/mash/{mash_Id}")
    public Optional<MashEntity> getMashById(@PathVariable String mash_Id) {
        return mashService.getMashById(mash_Id);

    }

    @DeleteMapping("/{mash_Id}")
    public void deleteValuesById(@PathVariable String mash_Id) {
        mashService.deleteMashById(mash_Id);
    }

}
