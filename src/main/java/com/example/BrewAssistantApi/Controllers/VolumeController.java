package com.example.BrewAssistantApi.Controllers;

import com.example.BrewAssistantApi.Dto.UpdateGravityDto;
import com.example.BrewAssistantApi.Dto.VolumeDto;
import com.example.BrewAssistantApi.Models.Volume;
import com.example.BrewAssistantApi.Services.VolumeService;
import com.example.BrewAssistantApi.Util.UtilVolume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/volume")
public class VolumeController {

    @Autowired
    private VolumeService volumeRepo;

    @GetMapping("/{recipe_id}")
    public VolumeDto getVolumeBuyRecipeId(@PathVariable String recipe_id){
        return UtilVolume.EntityToDto(volumeRepo.getVolumeFromUserId(recipe_id));
    }

    @PutMapping("/update/{recipe_id}")
    public VolumeDto updateVolumeBuyRecipeId(@PathVariable String recipe_id,@RequestBody Volume volume){
        return UtilVolume.EntityToDto(volumeRepo.updateVolumeFromUserId(volume,recipe_id));
    }
    @PutMapping("/update_gravity/{recipe_id}")
    public VolumeDto updateGravity(@PathVariable String recipe_id,@RequestBody UpdateGravityDto gravity){
        return UtilVolume.EntityToDto(volumeRepo.updateGravity(gravity,recipe_id));
    }
}
