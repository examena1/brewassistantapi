package com.example.BrewAssistantApi.Util;

import com.example.BrewAssistantApi.Dto.TimerDto;
import com.example.BrewAssistantApi.Entities.TimerEntity;
import com.example.BrewAssistantApi.Models.Timer;
import org.springframework.beans.BeanUtils;

public class UtilTimer {
    public static TimerEntity ModelToEntity(Timer timer){
        TimerEntity entity = new TimerEntity();
        BeanUtils.copyProperties(timer, entity);
        return entity;
    }
    public static TimerDto EntityToDto(TimerEntity timerEntity){
        TimerDto timer = new TimerDto();
        BeanUtils.copyProperties(timerEntity, timer);
        return timer;
    }
}
