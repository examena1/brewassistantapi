package com.example.BrewAssistantApi.Util;

import com.example.BrewAssistantApi.Dto.UserDto;
import com.example.BrewAssistantApi.Entities.UserEntity;
import com.example.BrewAssistantApi.Models.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;


public class UtilUser {

    public static UserEntity ModelToEntity(User user) {
        UserEntity entity = new UserEntity();
        BeanUtils.copyProperties(user, entity);
        return entity;
    }

    public static UserDto EntityToDto(UserEntity user) {
        UserDto model = new UserDto();
        BeanUtils.copyProperties(user, model);
        return model;
    }

}
