package com.example.BrewAssistantApi.Util;

import com.example.BrewAssistantApi.Dto.MaltDto;
import com.example.BrewAssistantApi.Dto.UserDto;
import com.example.BrewAssistantApi.Entities.MaltEntity;
import com.example.BrewAssistantApi.Entities.UserEntity;
import com.example.BrewAssistantApi.Models.Malt;
import lombok.Data;
import org.springframework.beans.BeanUtils;


public class UtilMalt {
    public static MaltEntity ModelToEntity(Malt malt){
        MaltEntity maltEntity = new MaltEntity();
        BeanUtils.copyProperties(malt, maltEntity);
        return maltEntity;
    }
//    public static MaltEntity ModelToEntity(Malt malt){
//        MaltEntity maltEntity = new MaltEntity(malt.malt, malt.batchNr, malt.parts,malt.gram);
//        BeanUtils.copyProperties(malt, maltEntity);
//        return maltEntity;
//    }
    public static MaltDto EntityToDto(MaltEntity malt){
        MaltDto maltDto = new MaltDto();
        BeanUtils.copyProperties(malt, maltDto);
        return maltDto;
    }



//    public static MaltDto EntityToDto(MaltEntity malt){
//        return new MaltDto(
//                malt.malt,
//                malt.batchNr,
//                malt.parts,
//                malt.gram
//        );
//    }
}
