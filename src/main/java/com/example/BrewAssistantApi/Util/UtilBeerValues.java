package com.example.BrewAssistantApi.Util;

import com.example.BrewAssistantApi.Dto.BeerValuesDto;
import com.example.BrewAssistantApi.Entities.BeerValuesEntity;
import com.example.BrewAssistantApi.Models.BeerValues;
import lombok.Data;
import org.springframework.beans.BeanUtils;


public class UtilBeerValues {

    public static BeerValuesEntity ModelToEntity(BeerValues beerValues){
        BeerValuesEntity beerValuesEntity = new BeerValuesEntity ();
        BeanUtils.copyProperties(beerValues, beerValuesEntity);
        return beerValuesEntity;
    }
    public static BeerValuesDto EntityToDto(BeerValuesEntity beerValuesEntity){
     return new BeerValuesDto(
             beerValuesEntity.beerName,
             beerValuesEntity.beerStyle,
             beerValuesEntity.batchNr,
             beerValuesEntity.goalIbu,
             beerValuesEntity.expectedGravity);
    }


}
