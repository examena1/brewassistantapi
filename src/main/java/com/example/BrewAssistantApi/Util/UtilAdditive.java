package com.example.BrewAssistantApi.Util;

import com.example.BrewAssistantApi.Dto.AdditivesDto;
import com.example.BrewAssistantApi.Entities.AdditiveEntity;
import com.example.BrewAssistantApi.Models.Additive;
import lombok.Data;
import org.springframework.beans.BeanUtils;


public class UtilAdditive {
    public static AdditiveEntity ModelToEntity(Additive additive){
       AdditiveEntity additiveEntity = new AdditiveEntity();
        BeanUtils.copyProperties(additive, additiveEntity);
        return additiveEntity;
    }

    public static AdditivesDto EntityToDto(AdditiveEntity additive){
       AdditivesDto additivesDto = new AdditivesDto();
        BeanUtils.copyProperties(additive, additivesDto);
        return additivesDto;
    }
}
