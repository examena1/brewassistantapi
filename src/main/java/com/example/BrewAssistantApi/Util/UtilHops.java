package com.example.BrewAssistantApi.Util;

import com.example.BrewAssistantApi.Dto.HopsDto;
import com.example.BrewAssistantApi.Entities.HopsEntity;
import com.example.BrewAssistantApi.Models.Hops;
import org.springframework.beans.BeanUtils;


public class UtilHops {
    public static HopsEntity ModelToEntity(Hops hops){
        HopsEntity hopsEntity = new HopsEntity();
        BeanUtils.copyProperties(hops, hopsEntity);
        return hopsEntity;
    }
    public static HopsDto EntityToDto(HopsEntity hopsEntity){
        HopsDto hopsDto = new HopsDto();
        BeanUtils.copyProperties(hopsEntity, hopsDto);
        return hopsDto;
    }
}
