package com.example.BrewAssistantApi.Util;

import com.example.BrewAssistantApi.Dto.MashDto;
import com.example.BrewAssistantApi.Entities.MashEntity;
import com.example.BrewAssistantApi.Models.Mash;
import org.springframework.beans.BeanUtils;

public class UtilMash {
    public static MashEntity ModelToEntity(Mash mash) {
        MashEntity mashEntity = new MashEntity();
        BeanUtils.copyProperties(mash, mashEntity);
        return mashEntity;
    }

    public static MashDto EntityToDto(MashEntity mashEntity) {
        return new MashDto(
                mashEntity.id,
                mashEntity.totalMalt,
                mashEntity.mashVolume,
                mashEntity.lauteringWater
        );
    }
}
