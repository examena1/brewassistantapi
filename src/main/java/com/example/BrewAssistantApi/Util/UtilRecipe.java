package com.example.BrewAssistantApi.Util;

import com.example.BrewAssistantApi.Dto.RecipeDto;
import com.example.BrewAssistantApi.Entities.RecipeEntity;
import com.example.BrewAssistantApi.Models.Recipe;
import lombok.Data;
import org.springframework.beans.BeanUtils;


public class UtilRecipe {

    public static RecipeEntity ModelToEntity(Recipe recipe) {
        RecipeEntity entity = new RecipeEntity();
        BeanUtils.copyProperties(recipe, entity);
        return entity;
    }

    public static RecipeDto EntityToDto(RecipeEntity recipe) {
        RecipeDto entity = new RecipeDto();
        BeanUtils.copyProperties(recipe, entity);
        return entity;

    }
//    public static RecipeDto EntityToDto(RecipeEntity recipe) {
//        return new RecipeDto(
//                recipe.beerValues,
//                recipe.additives,
//                recipe.malts,
//                recipe.hops,
//                recipe.volumeEntity,
//                recipe.timers,
//                recipe.note
//        );
//
//    }


}
