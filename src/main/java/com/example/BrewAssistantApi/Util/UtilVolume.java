package com.example.BrewAssistantApi.Util;

import com.example.BrewAssistantApi.Dto.VolumeDto;
import com.example.BrewAssistantApi.Entities.VolumeEntity;
import com.example.BrewAssistantApi.Models.Volume;
import org.springframework.beans.BeanUtils;

public class UtilVolume {
    public static VolumeEntity ModelToEntity(Volume volume){
        VolumeEntity  volumeEntity  = new VolumeEntity ();
        BeanUtils.copyProperties(volume, volumeEntity );
        return volumeEntity;
    }

    public static VolumeDto EntityToDto(VolumeEntity  volumeEntity){
        VolumeDto volumeDto = new VolumeDto();
        BeanUtils.copyProperties(volumeEntity, volumeDto);
        return volumeDto;
    }

    public static VolumeEntity EntityToEntity(VolumeEntity volume){
        VolumeEntity  volumeEntity  = new VolumeEntity ();
        BeanUtils.copyProperties(volume, volumeEntity );
        return volumeEntity;
    }
}
