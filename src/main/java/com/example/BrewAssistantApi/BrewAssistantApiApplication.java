package com.example.BrewAssistantApi;

import com.example.BrewAssistantApi.Entities.*;
import com.example.BrewAssistantApi.Repositories.*;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

@SpringBootApplication
public class BrewAssistantApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BrewAssistantApiApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(
            UserRepo userRepo, RecipeRepo recipeRepo, BeerValuesRepo beerRepo, VolumeRepo volumeRepo, MashRepo mashRepo
    ) {
        return args -> {

            UserEntity userEntity = new UserEntity("1",  "ismail@gmail.com", null);
            userRepo.save(userEntity);


            RecipeEntity recipeEntity = new RecipeEntity(
                    "2",
                    userEntity,
                    null,
                    null,
                    new ArrayList<AdditiveEntity>(),
                    new ArrayList<MaltEntity>(),
                    new ArrayList<HopsEntity>(),
                    null, new ArrayList<TimerEntity>()
                    , "test");
            //create initial volumeEntity and connect to recipe
            VolumeEntity volumeEntity = new VolumeEntity("3", 0.0, 0.0, 0.0, 0.0, recipeEntity);
            recipeEntity.volumeEntity = volumeEntity;


            //create initial mashEntity and connect to recipe
            MashEntity mashEntity = new MashEntity("4", 0.0, 0.0, 0.0, recipeEntity);
            recipeEntity.mashVolume = mashEntity;

            //create initial beerValues and connect to recipe
            BeerValuesEntity beerValuesEntity = new BeerValuesEntity("5", "", "", "", 0.0, 0.0, recipeEntity);
            recipeEntity.beerValues = beerValuesEntity;

            //Save entity to database
            mashRepo.save(mashEntity);
            beerRepo.save(beerValuesEntity);
            volumeRepo.save(volumeEntity);
//            recipeRepo.save(recipeEntity);


        };

    }


}
