package com.example.BrewAssistantApi.Dto;

import lombok.Data;

@Data
public class UpdateGravityDto {
    public Double gravity;
}


