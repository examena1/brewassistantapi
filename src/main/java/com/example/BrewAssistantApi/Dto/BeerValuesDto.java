package com.example.BrewAssistantApi.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BeerValuesDto {
    public String beerName;
    public String beerStyle;
    public String batchNr;
    public Double goalIbu;
    public Double expectedGravity;
}
