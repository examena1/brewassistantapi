package com.example.BrewAssistantApi.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HopsDto {
    public String hops;
    public String batchNr;
    public double alphaAcid;
    public double parts;
    public double gram;
}
