package com.example.BrewAssistantApi.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MaltDto {
    public  String  malt;
    public  String batchNr;
    public  double parts;
    public  double gram;
}
