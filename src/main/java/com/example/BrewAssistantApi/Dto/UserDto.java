package com.example.BrewAssistantApi.Dto;

import com.example.BrewAssistantApi.Entities.RecipeEntity;
import com.example.BrewAssistantApi.Entities.UserEntity;
import com.example.BrewAssistantApi.Models.Recipe;
import lombok.Data;

import java.util.List;

@Data
public class UserDto {
    public String id;
    public String email;
}
