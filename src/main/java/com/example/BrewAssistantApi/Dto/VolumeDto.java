package com.example.BrewAssistantApi.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VolumeDto {
    public double breBoil;
    public double afterBoil;
    public double gravityAfterBoiling;
    public double newVolume;
}
