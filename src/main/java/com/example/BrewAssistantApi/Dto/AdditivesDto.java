package com.example.BrewAssistantApi.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class AdditivesDto {
    public  String  id;
    public  String  additive;
    public  String batchNr;
    public  String property;
    public  double gram;


}
