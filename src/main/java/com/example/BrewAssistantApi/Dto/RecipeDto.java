package com.example.BrewAssistantApi.Dto;

import com.example.BrewAssistantApi.Entities.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecipeDto {
    public BeerValuesEntity beerValues;
    public MashEntity mashVolume;
    public List<AdditiveEntity> additives;
    public List<MaltEntity> malts;
    public List<HopsEntity> hops;
    public VolumeEntity volumeEntity;
    public List<TimerEntity> timers;
    public String note;


}
