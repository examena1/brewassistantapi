package com.example.BrewAssistantApi.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MashDto {
    public String id;
    public double totalMalt;
    public double mashVolume;
    public double lauteringWater;
}
