package com.example.BrewAssistantApi.Services;

import com.example.BrewAssistantApi.Entities.AdditiveEntity;
import com.example.BrewAssistantApi.Entities.MaltEntity;
import com.example.BrewAssistantApi.Entities.MashEntity;
import com.example.BrewAssistantApi.Entities.RecipeEntity;
import com.example.BrewAssistantApi.Models.Additive;
import com.example.BrewAssistantApi.Models.Malt;
import com.example.BrewAssistantApi.Repositories.AdditiveRepo;
import com.example.BrewAssistantApi.Repositories.MaltRepo;
import com.example.BrewAssistantApi.Repositories.MashRepo;
import com.example.BrewAssistantApi.Repositories.RecipeRepo;
import com.example.BrewAssistantApi.Util.UtilAdditive;
import com.example.BrewAssistantApi.Util.UtilMalt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class MaltService {
    @Autowired
    private RecipeRepo recipeRepo;
    @Autowired
    private MaltRepo maltRepo;
    @Autowired
    private MashService mashService;

    public Optional<MaltEntity> createMalt(Malt malt, String recipe_Id){
        Optional<String> recId = Optional.ofNullable(recipe_Id);
        RecipeEntity recipeEntity= recipeRepo.findById(recId.orElseThrow(() -> new NullPointerException("recipe_Id is null value "))).get();
        MaltEntity maltEntity = UtilMalt.ModelToEntity(malt);
        maltEntity.id = UUID.randomUUID().toString();

        //formel för mängd malt baserad på total önskad matlt och procentuell delmalt per maltsort.
        //nesta steg blir att uppdatera alla exissiterande malter om totalmalten ändras i mashEntity
        String mash_id =   recipeEntity.mashVolume.id;
        MashEntity mash =mashService.getMashById( mash_id).get();
        maltEntity.gram =   (mash.totalMalt * maltEntity.parts)/100;
        maltEntity.setRecipe(recipeEntity);
        maltRepo.save(maltEntity);

        return Optional.of(maltEntity);
    }

    public Stream<MaltEntity> getAllMaltsFromRecipe(String recipe_Id){
        List<MaltEntity> additiveEntities =maltRepo.findAllMaltaByRecipeId(recipe_Id);

        return  additiveEntities.stream();
    }

    public Optional<Double> totaMaltFromMaltList(String recipe_Id){
      return Optional.of(maltRepo.findAllMaltaByRecipeId(recipe_Id)
              .stream()
              .mapToDouble(MaltEntity::getGram)
              .sum());
    };

    public void deleteMalt(String malt_id) {
        maltRepo.deleteById(malt_id);
    }
}
