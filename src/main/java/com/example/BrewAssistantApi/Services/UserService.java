package com.example.BrewAssistantApi.Services;

import com.example.BrewAssistantApi.Entities.UserEntity;
import com.example.BrewAssistantApi.Models.User;
import com.example.BrewAssistantApi.Repositories.UserRepo;
import com.example.BrewAssistantApi.Util.UtilUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;


    public Optional<UserEntity> createUser(User user){
       UserEntity userEntity = UtilUser.ModelToEntity(user);
        userEntity .id = UUID.randomUUID().toString();
        userRepo.save(userEntity);
        return Optional.of(userEntity);
    }
    public Optional<UserEntity> updateUser(User user,String userId){

        Optional<String> optionalEmail = Optional.ofNullable(user.email);

        UserEntity userEntity = userRepo.findById(userId).orElseThrow(NullPointerException::new);
        userEntity.email= optionalEmail.isPresent()? user.email: userEntity.email;
        userRepo.save(userEntity);

        return Optional.of(userEntity);
    }


    public Optional<UserEntity> getUserById(String userId){
        UserEntity userEntity = userRepo.findById(userId).orElse(null);
        return Optional.of(userEntity);
    }

    public Optional<UserEntity> getUserByEmail(String email){
        UserEntity userEntity = userRepo.findUserEntityByEmail(email);
        return Optional.of(userEntity);
    }

    public Stream<UserEntity> getAllUsers() {
      return userRepo.findAll().stream();
    }



    public void deleteUserById(String id) {userRepo.deleteById(id);}
}
