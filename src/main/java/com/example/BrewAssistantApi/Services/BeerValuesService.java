package com.example.BrewAssistantApi.Services;

import com.example.BrewAssistantApi.Entities.BeerValuesEntity;
import com.example.BrewAssistantApi.Entities.RecipeEntity;
import com.example.BrewAssistantApi.Util.UtilBeerValues;
import com.example.BrewAssistantApi.Models.BeerValues;
import com.example.BrewAssistantApi.Repositories.BeerValuesRepo;
import com.example.BrewAssistantApi.Repositories.RecipeRepo;
import com.example.BrewAssistantApi.Repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class BeerValuesService {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private RecipeRepo recipeRepo;
    @Autowired
    private BeerValuesRepo beerValuesRepo;


    public Optional<BeerValuesEntity> createBeerValues(BeerValues beerValues, String recipeId) {
        Optional<String> resId = Optional.ofNullable(recipeId);
        RecipeEntity recipeEntity = recipeRepo.getById(resId.orElseThrow(() -> new NullPointerException("recipeId is null value ")));
        BeerValuesEntity beerValuesEntity = UtilBeerValues.ModelToEntity(beerValues);
        beerValuesEntity.id = UUID.randomUUID().toString();
        beerValuesEntity.setRecipe(recipeEntity);
        beerValuesRepo.save(beerValuesEntity);
        return Optional.of(beerValuesEntity);
    }

    public Optional<BeerValuesEntity> updateBeerValues(BeerValues beerValues, String recipe_Id) {
        Optional<BeerValues> beerValuesOptional = Optional.ofNullable(beerValues);
        Optional<String> resId = Optional.ofNullable(recipe_Id);
        BeerValuesEntity beerValuesEntity = beerValuesRepo.findBeerValuesByRecipeId(resId.orElseThrow(() -> new NullPointerException("beer_values_Id is null value ")));


        beerValuesOptional.map(data -> {
            beerValuesEntity.beerName = data.beerName.isBlank()||data.beerName ==null ||data.beerName.equals("string")?  beerValuesEntity.beerName:data.beerName;
            beerValuesEntity.beerStyle = data.beerStyle.isBlank() ||data.beerName ==null||data.beerStyle.equals("string")?beerValuesEntity.beerStyle:data.beerStyle;
            beerValuesEntity.batchNr = data.batchNr.isBlank() ||data.batchNr ==null||data.batchNr.equals("string")?beerValuesEntity.batchNr:data.batchNr;
            beerValuesEntity.expectedGravity = data.expectedGravity.isNaN() ?beerValuesEntity.expectedGravity:data.expectedGravity;
            beerValuesEntity.goalIbu = data.goalIbu.isNaN() ?beerValuesEntity.goalIbu:data.goalIbu;

            return null;
        });

        beerValuesRepo.save(beerValuesEntity);
        return Optional.of(beerValuesEntity);
    }

    public Stream<BeerValuesEntity> getAllBeerValues() {
        return beerValuesRepo.findAll().stream();
    }


    public Optional<BeerValuesEntity> getBeerValuesById(String id) {
        return beerValuesRepo.findById(id);
    }
    public void  deleteValuesById(String id) {
       beerValuesRepo.deleteById(id);
    }
}
