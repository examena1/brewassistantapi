package com.example.BrewAssistantApi.Services;

import com.example.BrewAssistantApi.Entities.AdditiveEntity;
import com.example.BrewAssistantApi.Entities.RecipeEntity;
import com.example.BrewAssistantApi.Models.Additive;
import com.example.BrewAssistantApi.Repositories.AdditiveRepo;
import com.example.BrewAssistantApi.Repositories.RecipeRepo;
import com.example.BrewAssistantApi.Util.UtilAdditive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class AdditiveService {
    @Autowired
    private AdditiveRepo additiveRepo;
    @Autowired
    private RecipeRepo recipeRepo;

    public Optional<AdditiveEntity> createAdditive(Additive additive, String recipe_Id){
        Optional<String> recId = Optional.ofNullable(recipe_Id);
        RecipeEntity recipeEntity= recipeRepo.findById(recId.orElseThrow(() -> new NullPointerException("recipe_Id is null value "))).get();
        AdditiveEntity additiveEntity = UtilAdditive.ModelToEntity(additive);
        additiveEntity.id = UUID.randomUUID().toString();
        additiveEntity.setRecipe(recipeEntity);
        additiveRepo.save(additiveEntity);
        return Optional.of(additiveEntity);
    }

    public Stream<AdditiveEntity> getAllAdditivesFromDb(){
        return  additiveRepo.findAll().stream();
    }

    public Stream<AdditiveEntity> getAllAdditivesFromRecipe(String recipe_Id){
//        Optional<String> recId = Optional.ofNullable(recipe_Id);
        List<AdditiveEntity> additiveEntities =additiveRepo.findAdditivesByRecipeId(recipe_Id);

        return  additiveEntities.stream();
    }

    public void deleteAdditive(String additive_id) {
        additiveRepo.deleteById(additive_id);
    }
}
