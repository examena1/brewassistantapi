package com.example.BrewAssistantApi.Services;

import com.example.BrewAssistantApi.Entities.*;
import com.example.BrewAssistantApi.Models.Hops;
import com.example.BrewAssistantApi.Models.Malt;
import com.example.BrewAssistantApi.Repositories.BeerValuesRepo;
import com.example.BrewAssistantApi.Repositories.HopsRepo;
import com.example.BrewAssistantApi.Repositories.RecipeRepo;
import com.example.BrewAssistantApi.Repositories.VolumeRepo;
import com.example.BrewAssistantApi.Util.UtilHops;
import com.example.BrewAssistantApi.Util.UtilMalt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class HopsService {
    @Autowired
    private RecipeRepo recipeRepo;
    @Autowired
    private HopsRepo hopsRepo;
    @Autowired
    private BeerValuesRepo beerValuesRepo;
    @Autowired
    private VolumeRepo volumeRepo;


    public Optional<HopsEntity> createHops(Hops hops, String recipe_Id) {
        Optional<String> recId = Optional.ofNullable(recipe_Id);
        RecipeEntity recipeEntity = recipeRepo.findById(recId.orElseThrow(() -> new NullPointerException("recipe_Id is null value "))).get();

        String beerValuesId = recipeEntity.beerValues.id;
        String mashVolumeId = recipeEntity.volumeEntity.id;
        VolumeEntity volumeEntity = volumeRepo.findById(mashVolumeId).get();
        BeerValuesEntity beerValuesEntity = beerValuesRepo.findById(beerValuesId).get();

//
//        goalIbu*0.96*varmvörtmed med spädvatten*procenthumlemängd/24/alphasyra/10
        // 35*0,96*2460*20 / 24 /14/10
        //spädvatten efter kok : önskad ibu*0.96 *slut volym * antal delar av humlemängd i procent /24/alphasyra
        HopsEntity hopsEntity = UtilHops.ModelToEntity(hops);
        hopsEntity.id = UUID.randomUUID().toString();
        hopsEntity.gram = beerValuesEntity.getGoalIbu()*0.96*volumeEntity.getNewVolume()*hops.parts/24/hops.alphaAcid/10 ;
        hopsEntity.setRecipe(recipeEntity);
        hopsRepo.save(hopsEntity);

        return Optional.of(hopsEntity);
    }

    public Stream<HopsEntity> getAllHopsFromRecipe(String recipe_Id) {
        List<HopsEntity> hopsEntities = hopsRepo.findHopsByRecipeId(recipe_Id);
        return hopsEntities.stream();
    }

    public Optional<Double> totalGramOfHopsFromList(String recipe_Id){
     return Optional.of(hopsRepo.findHopsByRecipeId(recipe_Id)
             .stream()
             .mapToDouble(HopsEntity::getGram)
             .sum());
    }

    public void deleteHops(String malt_id) {
        hopsRepo.deleteById(malt_id);
    }

}

