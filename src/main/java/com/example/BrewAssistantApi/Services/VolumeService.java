package com.example.BrewAssistantApi.Services;

import com.example.BrewAssistantApi.Dto.UpdateGravityDto;
import com.example.BrewAssistantApi.Entities.VolumeEntity;
import com.example.BrewAssistantApi.Models.Volume;
import com.example.BrewAssistantApi.Repositories.VolumeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VolumeService {

    @Autowired
    private VolumeRepo volumeRepo;

    public VolumeEntity getVolumeFromUserId(String recipe_Id) {
        return  volumeRepo.findVolumeByRecipeId(recipe_Id);
    };


    public VolumeEntity updateVolumeFromUserId(Volume volume, String userId) {
        VolumeEntity volumeEntity =volumeRepo.findVolumeByRecipeId(userId);
        volumeEntity.newVolume= volume.newVolume==0 ? volumeEntity.newVolume:volume.newVolume;
        volumeEntity.breBoil= volume.breBoil==0 ? volumeEntity.breBoil:volume.breBoil;
//        volumeEntity.newVolume= volume.newVolume==0 ? volumeEntity.newVolume:volume.newVolume;

        volumeRepo.save(volumeEntity);
        return volumeEntity;
    };

  public VolumeEntity updateGravity(UpdateGravityDto gravity, String userId) {
        VolumeEntity volumeEntity =volumeRepo.findVolumeByRecipeId(userId);
        volumeEntity.gravityAfterBoiling= gravity.gravity;

        volumeRepo.save(volumeEntity);
        return volumeEntity;
    };





}
