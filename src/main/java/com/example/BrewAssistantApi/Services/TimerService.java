package com.example.BrewAssistantApi.Services;

import com.example.BrewAssistantApi.Entities.RecipeEntity;
import com.example.BrewAssistantApi.Entities.TimerEntity;
import com.example.BrewAssistantApi.Models.Timer;
import com.example.BrewAssistantApi.Repositories.RecipeRepo;
import com.example.BrewAssistantApi.Repositories.TimerRepo;
import com.example.BrewAssistantApi.Util.UtilTimer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class TimerService {
    @Autowired
    private RecipeRepo recipeRepo;
    @Autowired
    private TimerRepo  timerRepo;



    public Optional<TimerEntity> createTimer(Timer hops, String recipe_Id){
        Optional<String> recId = Optional.ofNullable(recipe_Id);
        RecipeEntity recipeEntity= recipeRepo.findById(recId.orElseThrow(() -> new NullPointerException("recipe_Id is null value "))).get();
      TimerEntity timerEntity = UtilTimer.ModelToEntity(hops);
        timerEntity.id = UUID.randomUUID().toString();
        timerEntity.setRecipe(recipeEntity);
        timerRepo.save(timerEntity);
        return Optional.of(timerEntity);
    }

    public Stream<TimerEntity> getAllTimersFromRecipe(String recipe_Id){
//        Optional<String> recId = Optional.ofNullable(recipe_Id);
        List<TimerEntity> timerEntityList =timerRepo.findTimersByRecipeId(recipe_Id);
        return  timerEntityList.stream();
    }


    public void deleteTimer(String malt_id) {
        timerRepo.deleteById(malt_id);
    }

}
