package com.example.BrewAssistantApi.Services;

import com.example.BrewAssistantApi.Entities.*;
import com.example.BrewAssistantApi.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class RecipeService {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private RecipeRepo recipeRepo;
    @Autowired
    private VolumeRepo volumeRepo;
    @Autowired
    private BeerValuesRepo beerValuesRepo;
    @Autowired
    private MashRepo mashRepo;

    public Optional<String> createRecipe(String userId) {
        //find user
        UserEntity userEntity = userRepo.getById(userId);

        //create new recipe and connect to user
        RecipeEntity recipeEntity = new RecipeEntity();
        recipeEntity.id = UUID.randomUUID().toString();
        recipeEntity.note = "";
        recipeEntity.setUser(userEntity);

        //create initial mashEntity and connect to recipe
        MashEntity mashEntity = new MashEntity(UUID.randomUUID().toString(), 0.0, 0.0, 0.0, recipeEntity);
        recipeEntity.mashVolume = mashEntity;

        //create initial beerValues and connect to recipe
        BeerValuesEntity beerValues = new BeerValuesEntity(UUID.randomUUID().toString(), "", "", "", 0.0, 0.0, recipeEntity);
        recipeEntity.beerValues = beerValues;

        //create initial volumeEntity and connect to recipe
        VolumeEntity volumeEntity = new VolumeEntity(UUID.randomUUID().toString(), 0.0, 0.0, 0.0, 0.0, recipeEntity);
        recipeEntity.volumeEntity = volumeEntity;

        //Save entity to database
        mashRepo.save(mashEntity);
        beerValuesRepo.save(beerValues);
        volumeRepo.save(volumeEntity);
//        recipeRepo.save(recipeEntity);

        return Optional.of(recipeEntity.id);
    }

    public List<RecipeEntity> geAllRecipeFromUser(String userId) {
        UserEntity userEntity = userRepo.getById(userId);
        List<RecipeEntity> recipeEntity = userEntity.getRecipes();
        return recipeEntity;
    }

    public List<RecipeEntity> allRecipesFromDb() {
        return recipeRepo.findAll();
    }

    public Stream<RecipeEntity> recipeFromDbById(String id) {
        return recipeRepo.findById(id).stream();
    }

    public Optional<RecipeEntity> getSingelRecipeById(String id) {
        return recipeRepo.findById(id);
    }

    public void deleteRecipe(String recipeId) {
        recipeRepo.deleteById(recipeId);
    }

    public String writeNote(String id, String writtenNote) {
        Optional<RecipeEntity> recipeEntity = recipeRepo.findById(id);
        String responseNote = recipeEntity.get().note = writtenNote;
        recipeRepo.save(recipeEntity.get());
        return responseNote;
    }
    public String setNoteEmptyString(String id) {
        Optional<RecipeEntity> recipeEntity = recipeRepo.findById(id);
        String responseNote = recipeEntity.get().note = "";
        recipeRepo.save(recipeEntity.get());
        return responseNote;
    }
  public String getNote(String id) {
     String recipeNote = recipeRepo.findById(id).get().note;
        return recipeNote;
    }

}
