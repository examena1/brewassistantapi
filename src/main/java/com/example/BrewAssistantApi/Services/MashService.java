package com.example.BrewAssistantApi.Services;

import com.example.BrewAssistantApi.Entities.MashEntity;
import com.example.BrewAssistantApi.Entities.RecipeEntity;
import com.example.BrewAssistantApi.Entities.VolumeEntity;
import com.example.BrewAssistantApi.Models.Mash;
import com.example.BrewAssistantApi.Repositories.MashRepo;
import com.example.BrewAssistantApi.Repositories.RecipeRepo;
import com.example.BrewAssistantApi.Repositories.VolumeRepo;
import com.example.BrewAssistantApi.Util.UtilMash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class MashService {
    @Autowired
    private MashRepo mashRepo;
    @Autowired
    private RecipeRepo recipeRepo;
    @Autowired
    private VolumeRepo volumeRepo;


    public Optional<MashEntity> createMashValues(Mash mash, String recipe_Id) {
        Optional<String> resId = Optional.ofNullable(recipe_Id);
        RecipeEntity recipeEntity = recipeRepo.getById(resId.orElseThrow(() -> new NullPointerException("recipeId is null value ")));
        MashEntity mashEntity = UtilMash.ModelToEntity(mash);
        mashEntity.id = UUID.randomUUID().toString();
        mashEntity.setRecipe(recipeEntity);

        mashRepo.save(mashEntity);
        return Optional.of(mashEntity);
    }

    public Optional<MashEntity> updateMash(Mash mash, String recipe_Id) {


        Optional<Mash> mashOptional = Optional.ofNullable(mash);
        Optional<String> resId = Optional.ofNullable(recipe_Id);
        MashEntity mashEntity = mashRepo.findMashByRecipeId(resId.orElseThrow(() -> new NullPointerException("recipe_Id is null value ")));

        mashOptional.map(data -> {
            mashEntity.totalMalt = data.totalMalt == 0 ? mashEntity.totalMalt : data.totalMalt;
            mashEntity.mashVolume = data.mashVolume == 0 ? mashEntity.mashVolume : data.mashVolume;
            mashEntity.lauteringWater = data.lauteringWater == 0 ? mashEntity.lauteringWater : data.lauteringWater;
            return null;
        });

        VolumeEntity volumeEntity = volumeRepo.findVolumeByRecipeId(recipe_Id);
        double breBoilValue = mash.mashVolume + mash.lauteringWater;
        volumeEntity.breBoil = breBoilValue;
        volumeEntity.afterBoil = breBoilValue - (breBoilValue * 5 / 100);

        //tillfälligt detta ska ändras
        volumeEntity.newVolume = mash.lauteringWater*5/100;
        volumeRepo.save(volumeEntity);

        mashRepo.save(mashEntity);
        return Optional.of(mashEntity);
    }

    public Optional<MashEntity> getMashById(String id) {
        return mashRepo.findById(id);
    }

    public void deleteMashById(String id) {
        mashRepo.deleteById(id);
    }
}
