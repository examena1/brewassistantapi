package com.example.BrewAssistantApi.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "recipe_table")
public class RecipeEntity {
    @Id
    @Column(name = "id")
    public String id;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(
            name = "user_id",
            nullable = false,
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "user_recipe_fk"
            )
    )
    private UserEntity user;

    @JsonManagedReference
    @OneToOne(mappedBy = "recipe",orphanRemoval = true)
    public BeerValuesEntity beerValues;

    @JsonManagedReference
    @OneToOne(mappedBy = "recipe",orphanRemoval = true)
    public MashEntity mashVolume;

    @JsonManagedReference
    @OneToMany(
            mappedBy = "recipe",
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    public List<AdditiveEntity> additives = new ArrayList<>();

    @JsonManagedReference
    @OneToMany(
            mappedBy = "recipe",
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    public List<MaltEntity> malts = new ArrayList<>();;


    @JsonManagedReference
    @OneToMany(
            mappedBy = "recipe",
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    public List<HopsEntity> hops = new ArrayList<>();;

    @OneToOne(mappedBy = "recipe",orphanRemoval = true)
    public VolumeEntity volumeEntity;

    @JsonManagedReference
    @OneToMany(
            mappedBy = "recipe",
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    public List<TimerEntity> timers = new ArrayList<>();;

    public String note;



}
