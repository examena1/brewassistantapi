package com.example.BrewAssistantApi.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "beervalues")
public class BeerValuesEntity {
    @Id
    @Column(name = "values_id")
    public String id;
    public String beerName;
    public String beerStyle;
    public String batchNr;
    public double goalIbu;
    public double expectedGravity;

    @JsonBackReference
    @OneToOne(
           cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    @JoinColumn(
            name = "recipe_id",
            referencedColumnName = "id"
    )
    private RecipeEntity recipe;



}
