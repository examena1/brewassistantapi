package com.example.BrewAssistantApi.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "malt")
public class MaltEntity {
    @Id
    @Column(name = "malt_id")
    public  String id;
    public  String  malt;
    public  String batchNr;
    public  double parts;

    public  double gram;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(
            name = "recipe_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "recipe_malt_fk")
    )
    private RecipeEntity recipe;


    public MaltEntity(String malt, String batchNr, double parts, double gram) {
        this.malt = malt;
        this.batchNr = batchNr;
        this.parts = parts;
        this.gram = gram;
    }
}
