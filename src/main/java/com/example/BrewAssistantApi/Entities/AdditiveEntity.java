package com.example.BrewAssistantApi.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "additive")
public class AdditiveEntity {
    @Id
    @Column(name = "additive_id")
    public String id;
    public String additive;
    public String batchNr;
    public String property;
    public double gram;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(
            name = "recipe_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "recipe_additive_fk")
    )
    private RecipeEntity recipe;


}
