package com.example.BrewAssistantApi.Entities;

import com.example.BrewAssistantApi.Models.Recipe;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
public class UserEntity {
    @Id
    @Column(name = "id")
    public String id;
    @Column(unique=true)
    public String email;

    @JsonManagedReference
    @OneToMany(
            mappedBy = "user",
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    public List<RecipeEntity> recipes;

}
