package com.example.BrewAssistantApi.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "mash_volume")
public class MashEntity {
    @Id
    @Column(name = "mash_id")
    public String id;
    public double totalMalt;
    public double mashVolume;
    public double lauteringWater;


    @JsonBackReference
    @OneToOne(
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    @JoinColumn(
            name = "recipe_id",
            referencedColumnName = "id"
    )
    private RecipeEntity recipe;
}
