package com.example.BrewAssistantApi.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="hops")
public class HopsEntity {
    @Id
    @Column(name = "hops_id")
    public String id;
    public String hops;
    public String batchNr;
    public double alphaAcid;
    public double parts;
    public double gram;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(
            name = "recipe_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "recipe_hopse_fk")
    )
    private RecipeEntity recipe;

}
