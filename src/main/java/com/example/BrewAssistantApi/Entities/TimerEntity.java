package com.example.BrewAssistantApi.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="timer")
public class TimerEntity {
    @Id
    @Column(name = "timer_id")
    public String id;
    public String color;
    public String title;
    public double time;
    public Integer number;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(
            name = "recipe_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "recipe_timer_fk")
    )
    private RecipeEntity recipe;
}
