package com.example.BrewAssistantApi.Repositories;

import com.example.BrewAssistantApi.Entities.AdditiveEntity;
import com.example.BrewAssistantApi.Entities.HopsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HopsRepo extends JpaRepository<HopsEntity,String> {
    @Query("select s from hops s where s.recipe.id = ?1")
    List<HopsEntity> findHopsByRecipeId(String recipe_Id);
}
