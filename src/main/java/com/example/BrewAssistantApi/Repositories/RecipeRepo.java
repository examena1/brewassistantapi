package com.example.BrewAssistantApi.Repositories;

import com.example.BrewAssistantApi.Entities.AdditiveEntity;
import com.example.BrewAssistantApi.Entities.RecipeEntity;
import com.example.BrewAssistantApi.Models.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RecipeRepo extends JpaRepository<RecipeEntity,String> {
    @Query("select s from recipe_table s where s.user.id = ?1")
    List<RecipeEntity> findAllRecipeId(String recipe_Id);
}
