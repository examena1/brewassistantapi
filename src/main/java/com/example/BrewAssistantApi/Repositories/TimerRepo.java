package com.example.BrewAssistantApi.Repositories;

import com.example.BrewAssistantApi.Entities.TimerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TimerRepo extends JpaRepository<TimerEntity,String> {
    @Query("select s from timer s where s.recipe.id = ?1")
    List<TimerEntity> findTimersByRecipeId(String recipe_Id);
}
