package com.example.BrewAssistantApi.Repositories;

import com.example.BrewAssistantApi.Entities.BeerValuesEntity;
import com.example.BrewAssistantApi.Entities.MashEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BeerValuesRepo extends JpaRepository<BeerValuesEntity,String> {
    @Query("select s from beervalues s where s.recipe.id = ?1")
    BeerValuesEntity findBeerValuesByRecipeId(String recipe_Id);
}
