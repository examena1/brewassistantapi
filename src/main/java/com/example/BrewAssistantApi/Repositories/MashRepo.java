package com.example.BrewAssistantApi.Repositories;

import com.example.BrewAssistantApi.Entities.MaltEntity;
import com.example.BrewAssistantApi.Entities.MashEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MashRepo extends JpaRepository<MashEntity,String> {
    @Query("select s from mash_volume s where s.recipe.id = ?1")
   MashEntity findMashByRecipeId(String recipe_Id);
}
