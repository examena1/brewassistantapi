package com.example.BrewAssistantApi.Repositories;

import com.example.BrewAssistantApi.Entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepo extends JpaRepository<UserEntity,String> {
    @Query("select s from users s where s.email = ?1")
    UserEntity findUserEntityByEmail(String email);
}
