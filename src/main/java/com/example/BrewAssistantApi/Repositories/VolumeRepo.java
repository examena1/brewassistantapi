package com.example.BrewAssistantApi.Repositories;

import com.example.BrewAssistantApi.Entities.AdditiveEntity;
import com.example.BrewAssistantApi.Entities.VolumeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VolumeRepo extends JpaRepository<VolumeEntity,String> {
    @Query("select s from volume s where s.recipe.id = ?1")
    VolumeEntity findVolumeByRecipeId(String recipe_Id);
}
