package com.example.BrewAssistantApi.Repositories;

import com.example.BrewAssistantApi.Entities.AdditiveEntity;
import com.example.BrewAssistantApi.Entities.MaltEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MaltRepo extends JpaRepository<MaltEntity,String> {
    @Query("select s from malt s where s.recipe.id = ?1")
    List<MaltEntity> findAllMaltaByRecipeId(String recipe_Id);
}
