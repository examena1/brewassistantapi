package com.example.BrewAssistantApi.Repositories;

import com.example.BrewAssistantApi.Entities.AdditiveEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdditiveRepo extends JpaRepository<AdditiveEntity,String> {
    @Query("select s from additive s where s.recipe.id = ?1")
 List<AdditiveEntity> findAdditivesByRecipeId(String recipe_Id);
}
